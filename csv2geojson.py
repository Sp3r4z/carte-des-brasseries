#!/usr/bin/env python3

import sys, csv, json, datetime
from geojson import Feature, FeatureCollection, Point

if len(sys.argv) < 2:
    sys.exit("Paramètres manquants")

inputFile = sys.argv[1]
outputFile = sys.argv[2]

features = []
with open(inputFile, newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')

    #Supprime CSV headers
    next(reader)
    
    for latitude, longitude, enseigne, l1_declaree, l1_normalisee, datemaj, dcret, siren, nic, l4_declaree, codpos, libcom in reader:
        if len(latitude.strip()):
            latitude, longitude = map(float, (latitude,longitude))
            if len(enseigne):
                nom = enseigne
            elif len(l1_declaree):
                nom = l1_declaree
            else:
                nom = l1_normalisee

            maj = int(datetime.datetime.strptime(datemaj,'%Y-%m-%dT%H:%M:%S').timestamp())
            crea = int(datetime.datetime.strptime(dcret,'%Y%m%d').timestamp())

            features.append(
                Feature(
                    geometry = Point((longitude, latitude)),
                    properties = {
                        'name': nom,
                        'address': l4_declaree,
                        'postcode': codpos,
                        'city': libcom.strip(),
                        'creation': crea,
                        'updated': maj,
                        'siret': siren+nic
                    }
                )
            )

collection = FeatureCollection(features)
with open(outputFile, "w") as f:
    f.write('%s' % collection)
