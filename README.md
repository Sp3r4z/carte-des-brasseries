# Carte des brasseries

Carte des brasseries existantes (ayant une existence légale) sur le territoire Français

## Ressources
- [Leaflet](https://leafletjs.com/)
- [Sirene](https://www.sirene.fr/) géocodé par [Le site national des adresses](https://adresse.data.gouv.fr/)
- [Hop logo](https://thenounproject.com/kantortegalsari/collection/beer-day-glyph-64/?i=2809625)

## Un problème ?
- Faire une issue sur ce dépôt
- M'envoyer un message sur Mastodon [Sp3r4z](https://mastodon.xyz/@Sp3r4z)
