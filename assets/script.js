const map = L.map('map', {
	preferCanvas: true
}).setView([46.515,2.188], 6);

let markers = L.markerClusterGroup();

L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
	attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap contributors</a> | <a href="https://www.sirene.fr/">Base Sirene ®</a> géocodé par <a href="https://adresse.data.gouv.fr/">BAN</a>',
	maxZoom: 19,
}).addTo(map)

fetch('brasseries.geojson')
.then(response => response.json())
.then(datas => {
	L.geoJSON(datas, {
		onEachFeature: onEachFeature,
	}).addTo(markers);

	map.addLayer(markers);
})

const onEachFeature = (feature, layer) => {
	if (feature.properties && feature.properties.name) {
		let prop = feature.properties,
		popup = `<span class='popupTitle'>${prop.name}</span>`

		popup += (prop.address) ? '<br>'+prop.address : ''
		popup += (prop.postcode) ? '<br>'+prop.postcode : ''
		popup += (prop.city) ? ' '+prop.city : ''
		popup += (prop.website) ? `<br><a href="//${prop.website}">${prop.website}</a>` : ''

		if (prop.creation) {
			let date = new Date(prop.creation * 1000).toLocaleDateString("fr")
			popup +=	`<br>Brasserie créée le : ${date}`
		}
		if (prop.updated) {
			let date = new Date(prop.updated * 1000).toLocaleDateString("fr")
			popup +=	`<br>Mise à jour le : ${date}`
		}

		layer.bindPopup(popup);
	}
},

customControlLocateMe = L.Control.extend({
	options: {
		position: 'topright'
	},

	onAdd: _ => {
		let locateMe = L.DomUtil.create('button')
		locateMe.innerHTML = "📍"
		locateMe.title = "Auto-localisation"
		locateMe.id = "autolocate"
		locateMe.onclick = ev => {
			L.DomEvent.stopPropagation(ev)

			navigator.geolocation.getCurrentPosition(position => {
				map.setView([position.coords.latitude, position.coords.longitude],13)
			})
		}
		return locateMe
	}
})

//navigator.geolocation needs HTTPS to works
if ( location.protocol == 'https:' ) {
	map.addControl(new customControlLocateMe());
}
